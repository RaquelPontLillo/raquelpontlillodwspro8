<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-plus"></span> Crear nuevo curso</h3>

<form action="index.php?ctl=alta-curso" method="POST">
    <div class="form-group">
        <label name="id">ID</label>
        <input name="id" type="text" class="form-control" readonly="readonly" value="<?php echo $params['id'] ?>" />
    </div>
    
    <div class="form-group">
        <label name="nombre">Nombre</label>
        <input name="nombre" type="text" class="form-control" placeholder="Título del curso" />
    </div>

    <div class="form-group">
        <label name="horas">Horas</label>
        <input name="horas" type="text" class="form-control" placeholder="Horas totales" />
    </div>
    
    <div class="form-group">
        <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
    </div>
</form>

<?php
$contenido = ob_get_clean();
$titulo = 'Crear curso';
include 'layout.php'
?>