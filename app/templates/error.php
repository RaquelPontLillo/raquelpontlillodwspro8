<?php ob_start() ?>

<h1><small><font face="Tahoma" color="red">Error 404</font></small> <br />
    Página no encontrada </h1>
          <br />
          <p>No se ha encontrado la página solicitada. Usa el botón <strong>atrás</strong> de tu navegador para volver a la página previa,</p>
          <p><strong>o utiliza este botón para volver a inicio.</strong></p>
            <a href="index.php?ctl=inicio" class="btn btn-lg btn-info btn-block"><i class="glyphicon glyphicon-home"></i></a>
        </div>
        <br />

<?php
$contenido = ob_get_clean();
$titulo = 'Error';
include 'layout.php'
?>
