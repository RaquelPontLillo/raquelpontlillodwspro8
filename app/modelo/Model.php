<?php

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
interface Model {
    public function createCurso($curso);
    public function readCursos();
    public function updateCurso($curso);
    public function deleteCurso($curso);
    public function searchCurso($id);
    public function idCurso();
    
    public function createAlumno($alumno);
    public function readAlumnos();
    public function updateAlumno($alumno);
    public function deleteAlumno($alumno);
    public function searchAlumno($id);
    public function idAlumno();
    
    public function instalar();
    public function desinstalar();
    public function existe();
}
