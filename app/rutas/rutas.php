<?php

$rutas = array(
    //rutas generales
    'inicio' => array('controller' =>'ControladorGeneral', 'action' =>'home'),
    'login'  => array('controller' =>'ControladorGeneral', 'action' =>'login'),
    'salir' => array('controller' =>'ControladorGeneral', 'action' => 'logout'),
    'error' => array('controller' =>'ControladorGeneral', 'action' => 'error'),
    
    //rutas de base de datos
    'instalar' => array('controller' =>'ControladorGeneral', 'action' => 'instalar'),
    'desinstalar' => array('controller' =>'ControladorGeneral', 'action' => 'desinstalar'),
    'configurar' => array('controller' =>'ControladorGeneral', 'action' => 'configurarBD'),
    'cambiar-base-datos' => array('controller' =>'ControladorGeneral', 'action' => 'cambiarBD'),
    
    //rutas de cursos
    'cursos' => array('controller' =>'ControladorCurso', 'action' =>'read'),
    'crear-curso' => array('controller' =>'ControladorCurso', 'action' =>'create'),
    'alta-curso' => array('controller' =>'ControladorCurso', 'action' =>'save'),
    'editar-curso' => array('controller' =>'ControladorCurso', 'action' =>'update'),
    'actualizar-curso' => array('controller' =>'ControladorCurso', 'action' =>'edit'),
    'buscar-curso' => array('controller' =>'ControladorCurso', 'action' =>'search'),
    'borrar-curso' => array('controller' =>'ControladorCurso', 'action' =>'delete'),
    
    //rutas de alumnos
    'alumnos' => array('controller' =>'ControladorAlumno', 'action' =>'read'),
    'crear-alumno' => array('controller' =>'ControladorAlumno', 'action' =>'create'),
    'alta-alumno' => array('controller' =>'ControladorAlumno', 'action' =>'save'),
    'editar-alumno' => array('controller' =>'ControladorAlumno', 'action' =>'update'),
    'actualizar-alumno' => array('controller' =>'ControladorAlumno', 'action' =>'edit'),
    'buscar-alumno' => array('controller' =>'ControladorAlumno', 'action' =>'search'),
    'borrar-alumno' => array('controller' =>'ControladorAlumno', 'action' =>'delete')
);

