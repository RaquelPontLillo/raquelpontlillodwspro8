<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-plus"></span> Crear nuevo alumno</h3>

<form action="index.php?ctl=alta-alumno" method="POST">
    <div class="form-group">
        <label name="id">ID</label>
        <input name="id" type="text" class="form-control" readonly="readonly" value="<?php echo $params['id'] ?>" />
    </div>
    
    <div class="form-group">
        <label name="nombre">Nombre</label>
        <input name="nombre" type="text" class="form-control" placeholder="Nombre del alumno" />
    </div>

    <div class="form-group">
        <label name="curso">Curso</label>
        <select name="curso" class="form-control">
            <option value="0">-- Escoge un curso --</option>
            <?php foreach ($params['cursos'] as $curso) : ?>
                <option value="<?php echo $curso->__GET('id') ?>">
                    <?php echo $curso->__GET('nombre') . " (" . $curso->__GET('horas') . "h)" ?>
                </option>
            <?php endforeach; ?>    
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
    </div>
</form>

<?php
$contenido = ob_get_clean();
$titulo = 'Crear alumno';
include 'layout.php'
?>