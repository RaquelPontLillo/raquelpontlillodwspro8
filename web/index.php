<?php
//Inicio de la sesión
session_start();

//Modelo y controladores
require_once __DIR__ . '/../app/modelo/Alumno.php';
require_once __DIR__ . '/../app/modelo/Curso.php';
require_once __DIR__ . '/../app/modelo/Mysql.php';
require_once __DIR__ . '/../app/modelo/Files.php';
require_once __DIR__ . '/../app/utils/Funciones.php';
require_once __DIR__ . '/../app/ControladorGeneral.php';
require_once __DIR__ . '/../app/ControladorCurso.php';
require_once __DIR__ . '/../app/ControladorAlumno.php';

//Enrutamiento
require_once __DIR__ . '/../app/rutas/rutas.php';

//Parseo de la ruta y control de acceso
if (isset($_SESSION["autenticar"]) && $_SESSION["autenticar"] == "autenticado") {
    if (isset($_GET['ctl'])) {
        if (isset($rutas[$_GET['ctl']])) {
            $ruta = $_GET['ctl'];
        } else {
            //Si la opción seleccionada no existe en el array de mapeo, mostramos pantalla de error
            $ruta = 'error';
        }
    } else {
        //Si no se ha seleccionado nada mostraremos pantalla de inicio
        $ruta = 'inicio';
    }
} else {
    $ruta = 'login';
}

$controlador = $rutas[$ruta];

// Ejecución del controlador asociado a la ruta
if (method_exists($controlador['controller'], $controlador['action'])) {
    call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {
    //Si el método no existe, mostramos pantalla de error
    call_user_func(array('ControladorGeneral', 'error'));
}