<!DOCTYPE html>
<html lang="es">
    <head>
        <title><?php echo $titulo ?> | Matrículas App. 2016-2017</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="shortcut icon" href="images/kandel.ico">
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <meta charset="UTF-8">
        <style>
            body {
                background: url("images/fondo.jpg"), repeat;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                            </button> 
                            <a class="navbar-brand" href="index.php?ctl=inicio"><img src="images/kandel.ico" /></a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="index.php?ctl=inicio"><span class="glyphicon glyphicon-home"></span> Inicio</a></li>
                                <li class="dropdown">
                                    <a href="index.php?ctl=cursos" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-book"></span> Gestión de cursos<strong class="caret"></strong></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="index.php?ctl=cursos"><span class="glyphicon glyphicon-eye-open"></span> Ver todos</a></li>
                                        <li><a href="index.php?ctl=crear-curso"><span class="glyphicon glyphicon-plus"></span> Crear nuevo</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="index.php?ctl=alumnos" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Gestión de alumnos<strong class="caret"></strong></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="index.php?ctl=alumnos"><span class="glyphicon glyphicon-eye-open"></span> Ver todos</a></li>
                                        <li><a href="index.php?ctl=crear-alumno"><span class="glyphicon glyphicon-plus"></span> Crear nuevo</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="index.php?ctl=bbdd" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-floppy-disk"></span> Gestión de base de datos<strong class="caret"></strong></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="index.php?ctl=instalar"><span class="glyphicon glyphicon-floppy-saved"></span> Instalar base de datos</a></li>
                                        <li><a href="index.php?ctl=desinstalar"><span class="glyphicon glyphicon-floppy-remove"></span> Desinstalar base de datos</a></li>
                                        <li><a href="index.php?ctl=configurar"><span class="glyphicon glyphicon-cog"></span> Cambiar base de datos</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="index.php?ctl=salir"><span class="glyphicon glyphicon-off"></span> Cerrar sesión</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    
                    <?php if (isset($params['mensaje'])) : ?>
                        <div class="alert alert-<?php echo $params['mensaje']['tipo'] ?> alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $params['mensaje']['texto'] ?>
                        </div>
                    <?php endif; ?>
                    <div class="jumbotron">
                        <?php echo $contenido ?>
                    </div>

                    <footer>
                        <?php
                        echo "<pre>" . Config::$empresa . " " . Config::$autor . " ";
                        echo Config::$anio . " " . Config::$fecha . "</pre>"
                        ?>
                    </footer>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </body>
</html>