<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-eye-open"></span> Ver lista de alumnos</h3>
<div class="row">
    <div class="col-md-6">
        <a href="index.php?ctl=crear-alumno" class="btn btn-info" data-toggle="tooltip" title="Crear alumno"><span class="glyphicon glyphicon-plus"></span></a>
    </div>
    <div class="col-md-6">
        <form action="index.php?ctl=buscar-alumno" method="POST" class="pull-right">
            <div class="input-group">
                <input name="id" type="text" class="form-control" placeholder="Buscar por ID">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </form>
    </div>
</div>
<table class="table table-striped">
    <thead>
    <th>ID</th>
    <th>Nombre</th>
    <th>ID curso</th>
    <th>Acción</th>
</thead>
<tbody>
    <?php foreach ($params['alumnos'] as $alumno) : ?>
        <tr>
            <td><?php echo $alumno->__GET('id') ?></td>
            <td><?php echo $alumno->__GET('nombre') ?></td>
            <td><?php echo $alumno->__GET('curso')->__GET('id') ?></td>
            <td>
                <?php if ($alumno->__GET('id') != "") : ?>
                    <a href="index.php?ctl=editar-alumno&id=<?php echo $alumno->__GET('id') ?>" class="btn btn-warning" data-toggle="tooltip" title="Editar alumno"><span class="glyphicon glyphicon-pencil"></span></a>
                    <span data-toggle="modal" data-target="#borrar-alumno-<?php echo $alumno->__GET('id') ?>">
                        <a href="#borrar-alumno-<?php echo $alumno->__GET('id') ?>" data-toggle="tooltip" title="Borrar alumno" class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </span>
                    <div class="modal fade" id="borrar-alumno-<?php echo $alumno->__GET('id') ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Borrar alumno «<?php echo $alumno->__GET('nombre') ?>»</h4>
                                </div>
                                <div class="modal-body">
                                    ¿Quieres borrar este alumno? Esta acción no puede deshacerse.
                                </div>
                                <div class="modal-footer">
                                    <a href="index.php?ctl=borrar-alumno&id=<?php echo $alumno->__GET('id') ?>" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>    
</tbody>
</table>

<?php
$contenido = ob_get_clean();
$titulo = 'Ver alumnos';
include 'layout.php'
?>
