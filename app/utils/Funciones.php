<?php

include_once __DIR__ . '/../Config.php';

function recoge($valor) {
    $resultado = "";

    if (isset($_REQUEST[$valor])) {
	$resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
    }

    return $resultado;
}

function modelo() {
    if (isset($_SESSION["modelo"])) {
	$modelo = $_SESSION["modelo"];
	switch ($modelo) {
	    case 'ficheros':
		return new Files();
	    case 'mysql':
		return new Mysql();
	    default:
		return null;
	}
    } else {
	return null;
    }
}
