<?php

include_once __DIR__ . '/Alumno.php';
include_once __DIR__ . '/Curso.php';
include_once __DIR__ . '/Model.php';

/**
 * Description of Files
 *
 * @author Raquel Pont <raquel.pont.lillo@gmail.com>
 */
class Files implements Model {

    private $falumnos = "../app/database/alumnos.csv";
    private $fcursos = "../app/database/cursos.csv";

    public function existe() {
	if (file_exists($this->falumnos) && file_exists($this->fcursos)) {
	    return true;
	} else {
	    return false;
	}
    }

    public function instalar() {
        if (!file_exists($this->fcursos)) {
            $archivo = fopen($this->fcursos, "w");
            fclose($archivo);
            $this->createCurso(new Curso(1, "Historia", 35));
            $this->createCurso(new Curso(2, "Ingles", 40));
            $this->createCurso(new Curso(3, "Fisica", 30));
            $this->createCurso(new Curso(4, "Lengua", 50));
            $this->createCurso(new Curso(5, "Matematicas", 25));
            $this->createCurso(new Curso(6, "Tecnologia", 15));
        }

        if (!file_exists($this->falumnos)) {
            $archivo = fopen($this->falumnos, "w");
            fclose($archivo);
            $this->createAlumno(new Alumno(1, "Marta", 1));
            $this->createAlumno(new Alumno(2, "Luis", 2));
            $this->createAlumno(new Alumno(3, "Carmen", 3));
            $this->createAlumno(new Alumno(4, "Carlos", 6));
            $this->createAlumno(new Alumno(5, "Marcos", 5));
            $this->createAlumno(new Alumno(6, "Eva", 4));
        }
    }

    public function desinstalar() {
        if (file_exists($this->fcursos)) {
            unlink($this->fcursos);
        }

        if (file_exists($this->falumnos)) {
            unlink($this->falumnos);
        }
    }

    public function createAlumno($alumno) {
        $f = fopen($this->falumnos, "a");
        $linea = $alumno->__GET('id') . ";"
                . $alumno->__GET('nombre') . ";"
                . $alumno->__GET('curso')->__GET('id') . "\r\n";
        fwrite($f, $linea);
        fclose($f);
    }

    public function readAlumnos() {
        $alumnos = array();

        if ($archivo = fopen($this->falumnos, "r")) {
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                $curso = new Curso($token[2], null, null);
                $alumno = new Alumno($token[0], $token[1], $curso);
                array_push($alumnos, $alumno);
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
        } else {
            //errores
        }

        return $alumnos;
    }

    public function createCurso($curso) {
        $fc = fopen($this->fcursos, "a");
        $linea = $curso->__GET('id') . ";"
                . $curso->__GET('nombre') . ";"
                . $curso->__GET('horas') . "\r\n";
        fwrite($fc, $linea);
        fclose($fc);
    }

    public function readCursos() {
        $cursos = array();

        if ($archivoc = fopen($this->fcursos, "r")) {
            $tokenc = fgetcsv($archivoc, 0, ";");
            while ($tokenc) {
                $curso = new Curso($tokenc[0], $tokenc[1], $tokenc[2]);
                array_push($cursos, $curso);
                $tokenc = fgetcsv($archivoc, 0, ";");
            }
            fclose($archivoc);
        } else {
            
        }

        return $cursos;
    }

    public function idCurso() {
        $cursos = $this->readCursos();
        $ultCurso = end($cursos);
        $ultID = $ultCurso->__GET('id');
        $ultID++;
        return $ultID;
    }

    public function idAlumno() {
        $alumnos = $this->readAlumnos();
        $ultAlumno = end($alumnos);
        $ultID = $ultAlumno->__GET('id');
        $ultID++;
        return $ultID;
    }

    public function deleteAlumno($alumno) {
        $id = $alumno->__GET('id');
        if ($archivo = fopen($this->falumnos, "r+")) {
            $f = fopen("../app/database/alumnos-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink("../app/database/alumnos.csv");
        rename("../app/database/alumnos-temp.csv", "../app/database/alumnos.csv");
    }

    public function deleteCurso($curso) {
        $id = $curso->__GET('id');
        if ($archivo = fopen($this->fcursos, "r+")) {
            $f = fopen("../app/database/cursos-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink("../app/database/cursos.csv");
        rename("../app/database/cursos-temp.csv", "../app/database/cursos.csv");
    }

    public function searchAlumno($id) {
        $alumno = null;
        if ($archivo = fopen($this->falumnos, "r")) {
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id == $token[0]) {
                    $curso = new Curso($token[2], null, null);
                    $alumno = new Alumno($token[0], $token[1], $curso);
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
        }
        return $alumno;
    }

    public function searchCurso($id) {
        $curso = null;
        if ($archivo = fopen($this->fcursos, "r")) {
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id == $token[0]) {
                    $curso = new Curso($token[0], $token[1], $token[2]);
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
        }
        return $curso;        
    }

    public function updateAlumno($alumno) {
        $id = $alumno->__GET('id');
        if ($archivo = fopen($this->falumnos, "r+")) {
            $f = fopen("../app/database/alumnos-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                } else {
                    $linea = array($alumno->__GET('id'), $alumno->__GET('nombre'), $alumno->__GET('curso')->__GET('id'));
                    fputcsv($f, $linea, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink("../app/database/alumnos.csv");
        rename("../app/database/alumnos-temp.csv", "../app/database/alumnos.csv");
    }

    public function updateCurso($curso) {
        $id = $curso->__GET('id');
        if ($archivo = fopen($this->fcursos, "r+")) {
            $f = fopen("../app/database/cursos-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                } else {
                    $linea = array($curso->__GET('id'), $curso->__GET('nombre'), $curso->__GET('horas'));
                    fputcsv($f, $linea, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink("../app/database/cursos.csv");
        rename("../app/database/cursos-temp.csv", "../app/database/cursos.csv");
    }
}
