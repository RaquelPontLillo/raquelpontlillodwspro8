<?php

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
class ControladorCurso {

    function read() {
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$params = array('cursos' => $modelo->readCursos());
		require __DIR__ . '/templates/readCursos.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function create() {
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$params = array('id' => $modelo->idCurso());
		require __DIR__ . '/templates/crearCurso.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function save() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$horas = recoge('horas');
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		if ($id == "" || $nombre == "" || $horas == "") {
		    $params = array('id' => $modelo->idCurso());
		    $params['mensaje'] = array('tipo' => 'danger', 'texto' => 'No puede haber campos vacíos. Rellénalos.');
		    require __DIR__ . '/templates/crearCurso.php';
		} else {
		    $curso = new Curso($id, $nombre, $horas);
		    $modelo->createCurso($curso);
		    $params = array('cursos' => $modelo->readCursos());
		    $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Curso ' . $nombre . ' creado con éxito.');
		    require __DIR__ . '/templates/readCursos.php';
		}
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function update() {
	$id = $_GET['id'];
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$params['curso'] = $modelo->searchCurso($id);
		require __DIR__ . '/templates/editarCurso.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function edit() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$horas = recoge('horas');
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		if ($id == "" || $nombre == "" || $horas == "") {
		    $params['curso'] = new Curso($id, $nombre, $horas);
		    $params['mensaje'] = array('tipo' => 'danger', 'texto' => 'No puede haber campos vacíos. Rellénalos.');
		    require __DIR__ . '/templates/editarCurso.php';
		} else {
		    $curso = new Curso($id, $nombre, $horas);
		    $modelo->updateCurso($curso);
		    $params = array('cursos' => $modelo->readCursos());
		    $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Curso ' . $nombre . ' actualizado con éxito.');
		    require __DIR__ . '/templates/readCursos.php';
		}
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function delete() {
	$id = recoge('id');
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$curso = $modelo->searchCurso($id);
		$modelo->deleteCurso($curso);
		$params = array('cursos' => $modelo->readCursos());
		$params['mensaje'] = array('tipo' => 'success', 'texto' => 'Curso ' . $curso->__GET('nombre') . ' eliminado con éxito.');
		require __DIR__ . '/templates/readCursos.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function search() {
	$id = recoge('id');
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$curso = $modelo->searchCurso($id);
		if ($curso == null) {
		    $params['cursos'] = array(new Curso("", "", ""));
		    $params['mensaje'] = array('tipo' => 'danger', 'texto' => 'No se encuentran coincidencias con el id introducido.');
		} else {
		    $params['cursos'] = array($modelo->searchCurso($id));
		}
		require __DIR__ . '/templates/readCursos.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }
}
