<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-pencil"></span> Editar alumno «<?php echo $params['alumno']->__GET('nombre') ?>»</h3>

<form action="index.php?ctl=actualizar-alumno" method="POST">
    <div class="form-group">
        <label name="id">ID</label>
        <input name="id" type="text" class="form-control" readonly="readonly" value="<?php echo $params['alumno']->__GET('id') ?>" />
    </div>
    
    <div class="form-group">
        <label name="nombre">Nombre</label>
        <input name="nombre" type="text" class="form-control" placeholder="Título del curso" value="<?php echo $params['alumno']->__GET('nombre') ?>" />
    </div>

    <div class="form-group">
        <label name="curso">Curso</label>
        <select name="curso" class="form-control">
            <option value="0">-- Escoge un curso --</option>
            <?php foreach ($params['cursos'] as $curso) : ?>
                <?php if ($params['alumno']->__GET('curso')->__GET('id') == $curso->__GET('id')): ?>           
                    <option value="<?php echo $curso->__GET('id') ?>" selected>
                <?php else: ?>
                    <option value="<?php echo $curso->__GET('id') ?>">
                <?php endif; ?>
                        <?php echo $curso->__GET('nombre') . " (" . $curso->__GET('horas') . "h)" ?>
                    </option>
            <?php endforeach; ?>    
        </select>
    </div>
    
    <div class="form-group">

    <div class="form-group">
        <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
    </div>
    </div>
</form>

<?php
$contenido = ob_get_clean();
$titulo = 'Editar alumno';
include 'layout.php'
?>
