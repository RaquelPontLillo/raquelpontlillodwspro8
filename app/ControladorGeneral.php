<?php

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
class ControladorGeneral {
    function home() {
        require __DIR__ . '/templates/inicio.php';
    }
    
    function error() {
        require __DIR__ . '/templates/error.php';
    }
            
    function instalar() {
        $modelo = modelo();
        $modelo->instalar();
        $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Base de datos ' . $_SESSION['modelo'] . ' instalada con datos de ejemplo.');
        
        require __DIR__ . '/templates/inicio.php';
    }
    
    function desinstalar() {
        $modelo = modelo();
        $modelo->desinstalar();
        $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Base de datos ' . $_SESSION['modelo'] . ' desinstalada con éxito.');
        
        require __DIR__ . '/templates/inicio.php';
    }
    
    function configurarBD() {
        if (isset($_SESSION['modelo'])) {
            $params = array('modelo' => $_SESSION['modelo']);
        }
       
        require __DIR__ . '/templates/configurarBD.php';
    }

    function cambiarBD() {
        $bd = recoge('bd');
        $_SESSION['modelo'] = $bd;
        $params = array('modelo' => $_SESSION['modelo']);
        $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Escogido ' . $bd . ' como sistema de almacenamiento.');
        require __DIR__ . '/templates/inicio.php';
    }

    function login() {
        $usuario = recoge("usuario");
        $password = recoge("password");
        if ($usuario === "root" && $password === "pass") {
            $_SESSION["autenticar"] = "autenticado"; 
            if (!isset($_SESSION['modelo'])) {
                require __DIR__.'/templates/configurarBD.php';
            } else {
                require __DIR__.'/templates/configurarBD.php';
            }
        } else {
           require __DIR__.'/templates/login.php';
        }
    }

    function logout() {
        unset($_SESSION["autenticar"]);
        //unset($_SESSION["modelo"]);
        session_destroy();

        require __DIR__ . '/templates/login.php';
    }

}
