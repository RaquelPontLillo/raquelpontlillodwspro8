<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-education"></span> Bienvenido a «Matrículas App. 2016-2017»</h3>
<br />
<h4><span class="glyphicon glyphicon-folder-open"></span>  Documentación del proyecto en PDF</h4>

<div class="embed-responsive embed-responsive-4by3">
  <embed class="embed-responsive-item" src="docs/documentacion.pdf" type="application/pdf" width="800" height="600"></embed>
</div>

<?php
$contenido = ob_get_clean();
$titulo = 'Inicio';
include 'layout.php'
?>

