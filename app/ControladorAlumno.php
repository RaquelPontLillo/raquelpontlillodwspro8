<?php

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
class ControladorAlumno {

    function read() {
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$params = array('alumnos' => $modelo->readAlumnos());
		require __DIR__ . '/templates/readAlumnos.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function create() {
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$params = array('id' => $modelo->idAlumno(), 'cursos' => $modelo->readCursos());
		require __DIR__ . '/templates/crearAlumno.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function save() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$idCurso = recoge('curso');
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		if ($id == "" || $nombre == "" || $idCurso == "") {
		    $params = array('id' => $modelo->idAlumno(), 'cursos' => $modelo->readCursos());
		    $params['mensaje'] = array('tipo' => 'danger', 'texto' => 'No puede haber campos vacíos. Rellénalos.');
		    require __DIR__ . '/templates/crearAlumno.php';
		} else {
		    $curso = new Curso($idCurso, null, null);
		    $alumno = new Alumno($id, $nombre, $curso);
		    $modelo = modelo();
		    $modelo->createAlumno($alumno);
		    $params = array('alumnos' => $modelo->readAlumnos());
		    $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Alumno ' . $nombre . ' creado con éxito.');
		    require __DIR__ . '/templates/readAlumnos.php';
		}
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function update() {
	$id = $_GET['id'];
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$params = array('alumno' => $modelo->searchAlumno($id), 'cursos' => $modelo->readCursos());
		require __DIR__ . '/templates/editarAlumno.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function edit() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$idCurso = recoge('curso');

	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		if ($id == "" || $nombre == "" || $idCurso == "") {
		    $params['alumno'] = new Alumno($id, $nombre, $idCurso);
		    $params['mensaje'] = array('tipo' => 'danger', 'texto' => 'No puede haber campos vacíos. Rellénalos.');
		    require __DIR__ . '/templates/editarAlumno.php';
		} else {
		    $curso = new Curso($idCurso, null, null);
		    $alumno = new Alumno($id, $nombre, $curso);
		    $modelo = modelo();
		    $modelo->updateAlumno($alumno);
		    $params = array('alumnos' => $modelo->readAlumnos());
		    $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Alumno ' . $nombre . ' actualizado con éxito.');
		    require __DIR__ . '/templates/readAlumnos.php';
		}
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function delete() {
	$id = recoge('id');
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$alumno = $modelo->searchAlumno($id);
		$modelo->deleteAlumno($alumno);
		$params = array('alumnos' => $modelo->readAlumnos());
		$params['mensaje'] = array('tipo' => 'success', 'texto' => 'Alumno ' . $alumno->__GET('nombre') . ' eliminado con éxito.');
		require __DIR__ . '/templates/readAlumnos.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

    function search() {
	$id = recoge('id');
	if (modelo() != null) {
	    $modelo = modelo();
	    if ($modelo->existe()) {
		$alumno = $modelo->searchAlumno($id);
		if ($alumno == null) {
		    $params['alumnos'] = array(new Alumno("", "", new Curso("", "", "")));
		    $params['mensaje'] = array('tipo' => 'danger', 'texto' => 'No se encuentran coincidencias con el id introducido.');
		} else {
		    $params['alumnos'] = array($modelo->searchAlumno($id));
		}
		require __DIR__ . '/templates/readAlumnos.php';
	    } else {
		require __DIR__ . '/templates/errorBD.php';
	    }
	} else {
	    require __DIR__ . '/templates/errorModelo.php';
	}
    }

}
