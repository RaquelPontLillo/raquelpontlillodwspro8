<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-eye-open"></span> Ver lista de cursos</h3>
<div class="row">
    <div class="col-md-6">
        <a href="index.php?ctl=crear-curso" class="btn btn-info" data-toggle="tooltip" title="Crear curso"><span class="glyphicon glyphicon-plus"></span></a>
    </div>
    <div class="col-md-6">
        <form action="index.php?ctl=buscar-curso" method="POST" class="pull-right">
            <div class="input-group">
                <input name="id" type="text" class="form-control" placeholder="Buscar por ID">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </form>
    </div>
</div>
<table class="table table-striped">
    <thead>
    <th>ID</th>
    <th>Nombre</th>
    <th>Horas</th>
    <th>Acción</th>
</thead>
<tbody>
    <?php foreach ($params['cursos'] as $curso) : ?>

        <tr>
            <td><?php echo $curso->__GET('id') ?></td>
            <td><?php echo $curso->__GET('nombre') ?></td>
            <td><?php echo $curso->__GET('horas') ?></td>
            <td>
                <?php if ($curso->__GET('id') != "") : ?>
                    <a href="index.php?ctl=editar-curso&id=<?php echo $curso->__GET('id') ?>" class="btn btn-warning" data-toggle="tooltip" title="Editar curso"><span class="glyphicon glyphicon-pencil"></span></a>
                    <span data-toggle="modal" data-target="#borrar-curso-<?php echo $curso->__GET('id') ?>">
                        <a href="#borrar-curso-<?php echo $curso->__GET('id') ?>" data-toggle="tooltip" title="Borrar curso" class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </span>
                    <div class="modal fade" id="borrar-curso-<?php echo $curso->__GET('id') ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Borrar curso «<?php echo $curso->__GET('nombre') ?>»</h4>
                                </div>
                                <div class="modal-body">
                                    ¿Quieres borrar este curso? Esta acción no puede deshacerse.
                                </div>
                                <div class="modal-footer">
                                    <a href="index.php?ctl=borrar-curso&id=<?php echo $curso->__GET('id') ?>" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>    
</tbody>
</table>

<?php
$contenido = ob_get_clean();
$titulo = 'Ver cursos';
include 'layout.php'
?>