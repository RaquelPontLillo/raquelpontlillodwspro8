<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-cog"></span> Escoger base de datos</h3>

<form action="index.php?ctl=cambiar-base-datos" method="POST">
    <div class="form-group">
        <?php if (isset($params['modelo'])) : ?>
            <?php if ($params['modelo'] == "ficheros"): ?>           
	<label class="radio-inline"><input name="bd" type="radio" value="ficheros" checked="checked" required><img src="images/csv.png" /></label>
                <label class="radio-inline"><input name="bd" type="radio" value="mysql" required><img src="images/mysql.png" /></label>
            <?php elseif ($params['modelo'] == "mysql") : ?>   
                <label class="radio-inline"><input name="bd" type="radio" value="ficheros" required><img src="images/csv.png" /></label>
                <label class="radio-inline"><input name="bd" type="radio" value="mysql" checked="checked" required><img src="images/mysql.png" /></label>
            <?php endif; ?>
        <?php else : ?> 
            <label class="radio-inline"><input name="bd" type="radio" value="ficheros" required><img src="images/csv.png" /></label>
            <label class="radio-inline"><input name="bd" type="radio" value="mysql" required><img src="images/mysql.png" /></label>
        <?php endif; ?>  
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
    </div>
</form>

<?php
$contenido = ob_get_clean();
$titulo = 'Escoger base de datos';
include 'layout.php'
?>