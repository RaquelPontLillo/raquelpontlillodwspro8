<?php ob_start() ?>

<h3><span class="glyphicon glyphicon-pencil"></span> Editar curso «<?php echo $params['curso']->__GET('nombre') ?>»</h3>

<form action="index.php?ctl=actualizar-curso" method="POST">
    <div class="form-group">
        <label name="id">ID</label>
        <input name="id" type="text" class="form-control" readonly="readonly" value="<?php echo $params['curso']->__GET('id') ?>" />
    </div>
    
    <div class="form-group">
        <label name="nombre">Nombre</label>
        <input name="nombre" type="text" class="form-control" placeholder="Título del curso" value="<?php echo $params['curso']->__GET('nombre') ?>" />
    </div>

    <div class="form-group">
        <label name="horas">Horas</label>
        <input name="horas" type="text" class="form-control" placeholder="Horas totales" value="<?php echo $params['curso']->__GET('horas') ?>" />
    </div>
    
    <div class="form-group">

    <div class="form-group">
        <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span></button>
    </div>
    </div>
</form>

<?php
$contenido = ob_get_clean();
$titulo = 'Editar curso';
include 'layout.php'
?>
