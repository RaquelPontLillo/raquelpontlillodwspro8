<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Acceder | Matrículas App. 2016-2017</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="shortcut icon" href="images/kandel.ico">
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <meta charset="UTF-8">
        <style>
            body {
                background: url("images/fondo.jpg"), repeat;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        <form class="form-signin" method="POST" action="index.php?ctl=login">
                            <h2 class="form-signin-heading"><span class="glyphicon glyphicon-lock"></span> Acceso restringido: identifícate</h2>
                            <label for="user" class="sr-only">Usuario</label>
                            <input type="text" id="user" name="usuario" class="form-control" placeholder="Nombre de usuario" value="root" required autofocus>
                            <label for="inputPassword" class="sr-only">Contraseña</label>
                            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" value="pass" required>
                            <button class="btn btn-lg btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-log-in"></span></button>
                        </form>
                    </div>

                    <footer>
                        <?php
                        echo "<pre>" . Config::$empresa . " " . Config::$autor . " ";
                        echo Config::$anio . " " . Config::$fecha . "</pre>"
                        ?>
                    </footer>
                </div>
            </div>
        </div>
    </body>
</html>